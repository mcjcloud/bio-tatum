//
//  ViewController.swift
//  Bio
//
//  Created by Brayden Cloud on 3/30/19.
//  Copyright © 2019 Brayden Cloud. All rights reserved.
//

import UIKit
import MTBBarcodeScanner

class ViewController: UIViewController, CardInfoDelegate {
    
    var dict = [String: String]()
    
    override func viewDidAppear(_ animated: Bool) {
        /*
        [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            if (success) {
            
            NSError *error = nil;
            [self.scanner startScanningWithResultBlock:^(NSArray *codes) {
            AVMetadataMachineReadableCodeObject *code = [codes firstObject];
            NSLog(@"Found code: %@", code.stringValue);
            
            [self.scanner stopScanning];
            } error:&error];
            
            } else {
            // The user denied access to the camera
            }
        }];
        */
        let scanner = (UIApplication.shared.delegate as! AppDelegate).scanner
        MTBBarcodeScanner.requestCameraPermission { success in
            do {
                try scanner?.startScanning(resultBlock: { (codeObjects) in
                    let code = codeObjects?.first
                    scanner?.stopScanning()
                    
                    var codeStr = String(describing: code!.stringValue!)
                    codeStr = String(codeStr[codeStr.index(codeStr.startIndex, offsetBy: 1)...]);
                    self.dict = self.parseData(data: codeStr);
                    
                    // render card info view
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let cardViewController = storyBoard.instantiateViewController(withIdentifier: "cardVC") as! CardInfoViewController
                    cardViewController.delegate = self
                    self.present(cardViewController, animated: true, completion: nil)
                });
            }
            catch {
                print("caught an error")
                return;
            }
        };
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // [[MTBBarcodeScanner alloc] initWithPreviewView:self.previewView];
        (UIApplication.shared.delegate as! AppDelegate).scanner = MTBBarcodeScanner(previewView: self.view);
        
    }

    func parseData(data: String) -> [String: String] {
        var dictionary = [String: String]()
        data.enumerateLines { line, _ in
            if (line.count < 4) {
                return;
            }
//            _ = line.substring(to:line.index(line.startIndex, offsetBy: 3))
            let index = line.index(line.startIndex, offsetBy: 3);
            let code = String(line[..<index]);
            let value = String(line[index...]);
            dictionary[code] = value;
        };
        return dictionary;
    }
    
    func getData() -> (firstName: String?, lastName: String?, dob: String?, eyeColor: String?) {
        let dic = self.dict;
        return (dic["DCT"], dic["DCS"], dic["DBB"], dic["DAY"]);
    }

}

