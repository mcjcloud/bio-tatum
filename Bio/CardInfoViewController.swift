//
//  CardInfoViewController.swift
//  Bio
//
//  Created by Brayden Cloud on 3/30/19.
//  Copyright © 2019 Brayden Cloud. All rights reserved.
//

import UIKit

protocol CardInfoDelegate {
    func getData() -> (firstName: String?, lastName: String?, dob: String?, eyeColor: String?);
}

class CardInfoViewController: UIViewController {
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var eyeColorLabel: UILabel!
    var delegate: CardInfoDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let data = self.delegate?.getData();
        print("data: \(data)")
        self.updateInfo(firstName: data?.firstName, lastName: data?.lastName, dob: data?.dob, eyeColor: data?.eyeColor);
    }
    
    func updateInfo(firstName: String?, lastName: String?, dob: String?, eyeColor: String?) {
        self.firstNameLabel.text = firstName;
        self.lastNameLabel.text = lastName;
        self.dobLabel.text = dob;
        self.eyeColorLabel.text = eyeColor;
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
